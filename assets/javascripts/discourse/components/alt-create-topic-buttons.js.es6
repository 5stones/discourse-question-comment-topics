import { default as computed } from "ember-addons/ember-computed-decorators";

export default Ember.Component.extend({

  @computed("category","tag")
  commentUrl(category, tag, ) {
    return this.url(category, tag, ['comment']);
  },

  @computed("category","tag")
  questionUrl(category, tag) {
    return this.url(category, tag, ['question']);
  },

  tagList(activeTag, additionalTags) {

  },

  url(category, activeTag, tagList) {

    // Appends the currently active (viewed) tag to the pre-filled tagList
    if (activeTag && !(tagList.includes(activeTag.id))) {
      // Only add the passed tag if it's not already in the array.
      // This prevents redundantly adding the `question` and `comment` tags
      // when clicking the associated button on that tags "archive"
      tagList.push(activeTag.id);
    }

    let queryString = "?tags=" + tagList;

    if (category) {
      queryString += "&category=" + category.slug
    }
    return "/new-topic" + queryString;
  }
});
