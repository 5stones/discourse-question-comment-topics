# name: discourse-plugin-question-comment-topics
# about: Replaces the standard New Topic button with two alternatives: Leave a Comment and Ask a Question Topics created with these new buttons are tagged automatically
# version: 0.0.1
# authors: 5 Stones
# url: https://gitlab.com/5stones/discourse-question-comment-topics.git

enabled_site_setting :question_comment_topics_enabled

register_asset "javascripts/discourse/components/alt-create-topic-buttons.js.es6"
register_asset "javascripts/discourse/templates/components/alt-create-topic-buttons.hbs"
register_asset "javascripts/discourse/templates/components/custom-url-link-button.hbs"
register_asset "javascripts/discourse/templates/components/d-navigation.hbs"
register_asset "javascripts/discourse/templates/full-page-search.hbs"
register_asset "javascripts/discourse/templates/tags/show.hbs"
register_asset "javascripts/discourse/helpers/is-comment.js.es6"
register_asset "javascripts/discourse/helpers/is-question.js.es6"
register_asset "javascripts/discourse/helpers/is-create-topic.js.es6"
