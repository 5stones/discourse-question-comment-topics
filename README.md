# Discourse Question/Comment Topics

This plugin replaces the `New Topic` button in the navigation bar with two alternative options:

 * Ask Question
 * Add Comment

 These buttons open the New Topic dialog that is prefilled with an associated tag of either `question` or `comment`

 ## Installation

 Follow the Discourse [installation guide](https://meta.discourse.org/t/install-a-plugin/19157 "Install PLugins in Discourse") using `https://gitlab.com/5stones/discourse-question-comment-topics.git`

 Once installed, enable the plugin from the Discourse admin panel
